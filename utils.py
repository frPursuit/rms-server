from http.client import UNAUTHORIZED, BAD_REQUEST
from typing import Optional

from fastapi import Response, Request, HTTPException
from starlette.datastructures import UploadFile
from starlette.responses import JSONResponse

from settings import AUTHORIZED_TOKENS

JSON_MIME_TYPE = "application/json"
AUTH_HEADER = "Authorization"
TOKEN_AUTH_SCHEME = "Bearer"
MESSAGE_FIELD = "Message"


def render_json(data, status_code: int = 200) -> Response:
    return JSONResponse(data, status_code=status_code)


def render_json_message(message: str, status_code: int = 200) -> Response:
    return render_json({MESSAGE_FIELD: message}, status_code)


def render_success_message() -> Response:
    return render_json_message("Operation completed successfully")


def get_auth_token(request: Request) -> str:
    authorization = request.headers.get(AUTH_HEADER, "")
    if authorization.startswith(f"{TOKEN_AUTH_SCHEME} "):
        return authorization[len(TOKEN_AUTH_SCHEME)+1:]
    return ""


def check_credentials(request: Request):
    auth_token = get_auth_token(request)
    if auth_token not in AUTHORIZED_TOKENS:
        raise HTTPException(UNAUTHORIZED)


async def get_upload_file(request: Request) -> UploadFile:
    form = await request.form()
    file: Optional[UploadFile] = None

    for value in form.values():
        if isinstance(value, UploadFile):
            if file is None:
                file = value
            else: raise HTTPException(BAD_REQUEST, "Only one file can be uploaded at a time")
        else: raise HTTPException(BAD_REQUEST, "Invalid form data")

    if file is None:
        raise HTTPException(BAD_REQUEST, "No file was specified")
    return file
