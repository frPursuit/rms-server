# RMS (RESTfully Managed Storage) server

**This project has been deprecated in favor of S3-based solutions such as [MinIO](https://github.com/minio/minio).**

This server can be used to upload, modify, and download files using a simple REST API.

## Configuring the environment

To configure environment variables, create a `.env` file based on `.env.example`.
