import mimetypes
import os
import traceback
from http.client import INTERNAL_SERVER_ERROR, BAD_REQUEST, NOT_FOUND, METHOD_NOT_ALLOWED

from fastapi import FastAPI, Request, Response, HTTPException
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.responses import FileResponse

import manager
from settings import STORAGE_ROOT, DISPLAY_VERSION, VERSION, APP_NAME
from utils import render_json_message, render_json, render_success_message, check_credentials, get_upload_file

app = FastAPI(title=APP_NAME, openapi_url=None, docs_url=None, redoc_url=None)


async def handle(request: Request, path: str) -> Response:
    check_credentials(request)

    if path.startswith('/'):
        path = path[1:]
    full_path = os.path.join(STORAGE_ROOT, path)
    if full_path is None or not os.path.abspath(full_path).startswith(os.path.abspath(STORAGE_ROOT)):
        raise HTTPException(BAD_REQUEST, "Invalid path")

    if request.method == "GET":
        if os.path.isdir(full_path):
            (files, dirs) = manager.list_directory(full_path)
            return render_json({
                "Files": files,
                "Directories": dirs,
                "Version": VERSION,
                "DisplayVersion": DISPLAY_VERSION
            })
        elif os.path.isfile(full_path):
            return FileResponse(full_path)
        else: raise HTTPException(NOT_FOUND)
    elif request.method == "HEAD":
        file_size = manager.get_file_size(full_path)
        headers = {}

        mimetype, encoding = mimetypes.guess_type(full_path)
        if mimetype is None:
            mimetype = "application/octet-stream"
        headers["Content-Type"] = mimetype

        if encoding is not None:
            headers["Content-Encoding"] = encoding

        headers["Content-Length"] = file_size
        return Response(headers=headers)  # Responses to HEAD requests shouldn't have a body
    elif request.method == "POST":
        manager.post_path(await get_upload_file(request), full_path)
        return render_success_message()
    elif request.method == "PUT":
        manager.put_path(await get_upload_file(request), full_path)
        return render_success_message()
    elif request.method == "DELETE":
        manager.delete_path(full_path)
        return render_success_message()
    else: raise HTTPException(METHOD_NOT_ALLOWED)


@app.api_route("/", methods=["GET", "HEAD", "POST", "PUT", "DELETE"])
async def handle_root(request: Request) -> Response:
    return await handle(request, "/")


@app.api_route("/{path:path}", methods=["GET", "HEAD", "POST", "PUT", "DELETE"])
async def handle_path(request: Request, path: str) -> Response:
    return await handle(request, path)


# Exception handling

@app.exception_handler(StarletteHTTPException)
async def handle_exception(request: Request, exception: StarletteHTTPException) -> Response:
    return render_json_message(f"Error {exception.status_code}: {exception.detail}", exception.status_code)


# Middleware

@app.middleware("http")
async def catch_exception(request: Request, call_next) -> Response:
    try:
        return await call_next(request)
    except Exception as e:
        traceback.print_exception(e)
        if isinstance(e, HTTPException):
            return await handle_exception(request, e)
        else: return await handle_exception(request, HTTPException(INTERNAL_SERVER_ERROR, str(e)))
