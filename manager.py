import os
import shutil
from http.client import BAD_REQUEST, NOT_FOUND
from typing import Tuple, List

from fastapi import HTTPException
from starlette.datastructures import UploadFile


def list_directory(path: str) -> Tuple[List[str], List[str]]:
    files = []
    dirs = []
    for entry in os.listdir(path):
        if os.path.isfile(os.path.join(path, entry)):
            files.append(entry)
        else: dirs.append(entry)
    return files, dirs


def get_file_size(path: str) -> int:
    if os.path.isdir(path):
        raise HTTPException(BAD_REQUEST, "The specified path is a directory")

    if os.path.isfile(path):
        return os.stat(path).st_size
    else: raise HTTPException(NOT_FOUND)


def put_path(file: UploadFile, path: str):
    if os.path.isdir(path):
        raise HTTPException(BAD_REQUEST, "The specified path is a directory")

    dirname = os.path.dirname(path)
    os.makedirs(dirname, exist_ok=True)

    with open(path, "wb") as saved_file:
        shutil.copyfileobj(file.file, saved_file)


def post_path(file: UploadFile, path: str):
    if os.path.isfile(path):
        raise HTTPException(BAD_REQUEST, "The specified file already exists")
    return put_path(file, path)


def delete_path(path: str):
    if os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.isfile(path):
        os.remove(path)
    else: raise HTTPException(NOT_FOUND)
