from pursuitlib.utils import get_env, is_null_or_empty


# Application settings

APP_NAME = "rms-server"

VERSION = "0.2.0"
DEV_STAGE = "BETA"
DISPLAY_VERSION = VERSION if DEV_STAGE is None else VERSION + " " + DEV_STAGE

STORAGE_ROOT = get_env("STORAGE_ROOT")

AUTHORIZED_TOKENS_FILE = get_env("AUTHORIZED_TOKENS_FILE")

with open(AUTHORIZED_TOKENS_FILE, "r") as file:
    valid_tokens = filter(lambda t: not is_null_or_empty(t.split("#")[0]), file.readlines())
    AUTHORIZED_TOKENS = list(map(lambda line: line.split("#")[0].replace("\n", "").replace("\r", ""), valid_tokens))
